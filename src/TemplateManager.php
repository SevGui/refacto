<?php

class TemplateManager
{
    public function getTemplateComputed(Template $tpl, array $data)
    {
        if (!$tpl) {
            throw new \RuntimeException('no tpl given');
        }

        $replaced = clone($tpl);
        $replaced->subject = $this->computeText($replaced->subject, $data);
        $replaced->content = $this->computeText($replaced->content, $data);

        return $replaced;
    }

    private function computeText($text, array $data)
    {
        /**
         * Processing for Quote
         * [quote:*]
         */
        if (isset($data['quote']) && $data['quote'] instanceof Quote) {
            $quote = QuoteRepository::getInstance()->getById($data['quote']->id);
            $text = $this->replaceTextForQuote($text, $quote);
        }

        /**
         * Processing for User
         * [user:*]
         */
        $applicationContext = ApplicationContext::getInstance();
        $user = (isset($data['user']) && ($data['user'] instanceof User)) ? $data['user'] : $applicationContext->getCurrentUser();
        $text = $this->replaceTextForUser( $text, $user);

        return $text;
    }

    /**
     * Replaces the Quote part in the text
     * @param $text
     * @param Quote $quote
     * @return string|string[]
     */
    private function replaceTextForQuote($text, Quote $quote)
    {
        // Look for all quote
        preg_match_all('/quote:([a-zA-Z0-9\_]+)/', $text, $matches);

        // Replace text
        $placeholders = Quote::$placeholders;
        foreach ($matches[1] as $match) {
            if ($placeholders[$match] !== null) {
                $replaceQuote = $quote->getReplaceText($match);
                $text = str_replace('[quote:' . $match . ']', $replaceQuote, $text);
            }
        }
        return $text;
    }

    /**
     * Replaces the User part in the text
     * @param $text
     * @param User $user
     * @return string|string[]
     */
    private function replaceTextForUser($text, User $user)
    {
        // Look for all user
        preg_match_all('/user:([a-zA-Z0-9\_]+)/', $text, $matches);

        // Replace text
        $placeholders = User::$placeholders;
        foreach ($matches[1] as $match) {
            if ($placeholders[$match] !== null) {
                $placeholder = User::$placeholders[$match];
                $replaceUser = $user->$placeholder;
                $text = str_replace('[user:' . $match . ']', $replaceUser, $text);
            }
        }
        return $text;
    }
}
